# Sort Order #

Implementation of sort order task

### Language ###

I spent most of my time on .net projects so my choice (C# and .net core 3.1) was simply love to this language.

### Build ###

* dotnet restore
* dotnet build -c Release -o App

### Run ###

In App folder run Task2.exe width height mines.
./Task1.exe "A<B" "B<C" "C<D"

### Implementation ###

* triming the data creating array of string looking like AB, BC, ...
* use first string as init for ordered result and exclude it from data set
* iterate through data set till matching first or last char of result and actual tuple is found
* place it at begining or end of result depending on match position and remove it from data set
* repeat until dataset is empty