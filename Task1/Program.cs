﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            ValidateInput(args);

            var trimed_data = new List<string>();
            

            foreach (var arg in args)
            {
                trimed_data.Add(arg[0].ToString() + arg[2].ToString());
            }

            var engine = new SortingEngine();
            Console.WriteLine(engine.Sort(trimed_data));
        }

        public static void ValidateInput(string[] args)
        {
            if (args.Count() == 0)
            {
                Console.WriteLine("Please enter valid input");
                Environment.Exit(0);
            }

            var regex = new Regex("^[A-Z]<[A-Z]$");
            foreach (var input in args)
            {
                if(!regex.IsMatch(input))
                {
                    Console.WriteLine("Please enter valid input");
                    Environment.Exit(0);
                }
            }
        }
    }
}
