﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task1
{
    public class SortingEngine
    {
        public string Sort(List<string> input)
        {
            var result = input.First();
            input.RemoveAt(0);

            while (input.Any())
            {
                for (int i = 0; i < input.Count(); i++)
                {
                    if (input[i][0] == result.Last())
                    {
                        result += input[i][1];
                        input.RemoveAt(i);
                        break;
                    }

                    if (input[i][1] == result.First())
                    {
                        result = input[i][0] + result;
                        input.RemoveAt(i);
                        break;
                    }
                }
            }

            return result;
        }
    }
}
