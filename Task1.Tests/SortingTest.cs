using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Task1.Tests
{
    public class SortingTest
    {
        [Theory]
        [InlineData(( new string[] { "AB", "BC", "CD", "DE" } ), "ABCDE")]
        [InlineData((new string[] { "CB", "BA", "GH", "AG" }), "CBAGH")]
        public void CorrectSortingEngineOutput(string[] input, string expectedResult)
        {
            var engine = new SortingEngine();
            var sortedResult = engine.Sort(input.ToList());

            Assert.Equal(expectedResult, sortedResult);
        }
    }
}
